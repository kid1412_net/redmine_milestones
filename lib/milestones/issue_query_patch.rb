module IssueQueryPatch
  def self.included(base) # :nodoc:
    #base.extend(ClassMethods)

    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development

      base.add_available_column(QueryColumn.new(:milestone, :sortable => "#{Issue.table_name}.milestone_id", :groupable => true))
    end
  end
end
